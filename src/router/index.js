import { createRouter, createWebHistory } from 'vue-router'
import Index from '../pages/Index.vue'
import Pokemon from '../pages/Pokemon.vue'

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/pokemon/:id',
    name: 'Pokemon',
    component: Pokemon
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
