export default class FetchRequest {
    constructor(url, method = 'GET', data = null) {
        this.url = url
        this.method = method
        this.data = data
    }

    async send() {
        const options = {
            method: this.method,
            mode: 'cors'
        }

        if (this._canSendBody()) {
            options.body = JSON.stringify(this.data)
            options.headers = { 'Content-Type': 'application/json' }
        }

        return fetch(this.url, options)
            .then(async response => {
                if (response.ok)
                    return await response.json()

                throw new Error(`An error occured: ${response.status}, ${response.statusText}`)
            })
    }

    _canSendBody() {
        return this.method === 'POST' || this.method === 'PUT'
    }
}